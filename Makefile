NAME1 = Colleen
NAME2 = Grace
NAME3 = Sully

SRCPATH1 = Colleen_dir
SRCPATH2 = Grace_dir
SRCPATH3 = Sully_dir
OBJPATH1 = $(SRCPATH1)/obj
OBJPATH2 = $(SRCPATH2)/obj
OBJPATH3 = $(SRCPATH3)/obj

CC = gcc
BASEFLAGS = -Wall -Wextra
CFLAGS = $(BASEFLAGS) -Werror -O2 -g

SRCSFILES1 = Colleen.c
SRCSFILES2 = Grace.c
SRCSFILES3 = Sully.c

SRC1 = $(addprefix $(SRCPATH1)/,$(SRCSFILES1))
SRC2 = $(addprefix $(SRCPATH2)/,$(SRCSFILES2))
SRC3 = $(addprefix $(SRCPATH3)/,$(SRCSFILES3))
OBJECTS1 = $(SRC1:$(SRCPATH1)/%.c=$(OBJPATH1)/%.o)
OBJECTS2 = $(SRC2:$(SRCPATH2)/%.c=$(OBJPATH2)/%.o)
OBJECTS3 = $(SRC3:$(SRCPATH3)/%.c=$(OBJPATH3)/%.o)

RM = rm -rf

# Yellow
Y = \033[0;33m
# Red
R = \033[0;31m
# Green
G = \033[0;32m
# No color
E = \033[39m

all: $(NAME1) $(NAME2) $(NAME3)

$(OBJECTS1): $(OBJPATH1)/%.o : $(SRCPATH1)/%.c
	@echo "$(Y)[CREATE OBJ DIR]$(E)"
	@mkdir -p $(dir $@)
	@$(CC) -o $@ $(CFLAGS) -c $<

$(OBJECTS2): $(OBJPATH2)/%.o : $(SRCPATH2)/%.c
	@echo "$(Y)[CREATE OBJ DIR]$(E)"
	@mkdir -p $(dir $@)
	@$(CC) -o $@ $(CFLAGS) -c $<

$(OBJECTS3): $(OBJPATH3)/%.o : $(SRCPATH3)/%.c
	@echo "$(Y)[CREATE OBJ DIR]$(E)"
	@mkdir -p $(dir $@)
	@$(CC) -o $@ $(CFLAGS) -c $<

$(NAME1): $(OBJECTS1)
	@echo "$(Y)[COLLEEN]$(E)"
	@echo "$(Y)[COMPILING]$(E)"
	@echo "$(G)$(CC) -o $@ $(CFLAGS) $(OBJECTS1) $(E)"
	@$(CC) -o $(SRCPATH1)/$@ $(CFLAGS) $(OBJECTS1)
	@echo "$(G)[COMPILING DONE]$(E)"

$(NAME2): $(OBJECTS2)
	@echo "$(Y)[GRACE]$(E)"
	@echo "$(Y)[COMPILING]$(E)"
	@echo "$(G)$(CC) -o $@ $(CFLAGS) $(OBJECTS2) $(E)"
	@$(CC) -o $(SRCPATH2)/$@ $(CFLAGS) $(OBJECTS2)
	@echo "$(G)[COMPILING DONE]$(E)"

$(NAME3): $(OBJECTS3)
	@echo "$(Y)[SULLY]$(E)"
	@echo "$(Y)[COMPILING]$(E)"
	@echo "$(G)$(CC) -o $@ $(CFLAGS) $(OBJECTS3) $(E)"
	@$(CC) -o $(SRCPATH3)/$@ $(CFLAGS) $(OBJECTS3)
	@echo "$(G)[COMPILING DONE]$(E)"

clean:
	@echo "$(R)[REMOVE OBJ DIR]$(E)"
	@$(RM) $(OBJPATH1)
	@$(RM) $(OBJPATH2)
	@$(RM) $(OBJPATH3)

fclean: clean
	@$(RM) $(SRCPATH1)/$(NAME1)
	@$(RM) $(SRCPATH2)/$(NAME2)
	@$(RM) $(SRCPATH3)/$(NAME3)

re: fclean all
